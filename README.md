## What is this ?
BORG BACKUP ( https://borgbackup.readthedocs.io/en/stable/ )
It's a great tool offering more than duplicity has and I think it is even more performant than duply in the end.

I created this little wrapper ontop of the BORG BACKUP PROJECT. The wrapper just simplifies my day to day work a lot.
As mentioned, I only added the "everyday" handling into the wrapper to make life just a little bit easier, if you need special stuff email me.

Basically this is just a tool who triggers borg-backup commands in the end.

Since this is the very first version - please contribute or report bugs you find !
After all, I hope you like it !


### What is borg backup
IF you don't know it, you might dont want this wrapper - but give it a try !!
But I won't tell you what it is - read here: [Borg Backup Website](https://borgbackup.readthedocs.io/en/stable/)

## Usage
1) Install borgbackup for your OS. [Read this for more instructions](https://borgbackup.readthedocs.io/en/stable/installation.html) Binary version has been tested, too. Configure the location of the binary in the project config file (at the bottom)
2) Modify the script to your needs (read the comments wisely!)  
3) Init the repository `borg_wrapper.sh -init `  
4) Create a backup `borg_wrapper.sh -c <YourProjectName>`  
5) Check creation of backup `borg_wrapper.sh -l `  

If this worked, you can add a cronjob to your crontab:
`00 01 * * * /path/to/borg_wrapper/borg_wrapper.sh -c <YourProjectName>`
This line wil trigger a backup with the given config at 01:00 am.

## Features

### Actions
`-h, --help`
Show the help

`-l, --list`  
List the projects repository content

`-I, --info`  
List infos of the backup artefact content 

`-i, --init`  
Initialize the projects repository  

`-c,--create`  
Trigger a new backup

`-p, --prune`  
Prune/Wipe a archive 

`-d, --del`  
Delete a archive or repository

`-v, --check `  
Verify repository consistency 



## Disclaimer
Me nor any contributor are taking any guarantee for this script, your backups or anything else.
Neither I'm taking responsability for loss of data, damage to your system or anything else. 
THIS IS NOT AN OFFICIAL BORG PROJECT - THIS IS DONE BY xjpalma.
Please verify the code yourself, before you start deploying to production.
Feel free to fork or report bugs or anything to us.
Suggestions are welcome, too !!!
