#!/bin/bash

# Base Variables
version="v1.0"

set -o errexit
set +o nounset
set -o pipefail


readonly USER="<USER>"  ## user to ssh 
readonly HOST="<HOST>"  ## IP, localhost to ssh
readonly REPO="<PATH_REPO>" # Path to repository on the server

readonly TARGET="${USER}@${HOST}:${REPO}"
export BORG_REPO=ssh://$TARGET

export BORG_RSH="ssh "


## Valid options are "none", "keyfile", and "repokey"
readonly ENCRYPTION_METHOD=none

## Compression algorithm and level. See Borg docs.
## No compression: --compression none
## Super fast, low compression (lz4, default)
## Less fast, higher compression (zlib, N = 0..9): --compression zlib,N
## Even slower, even higher compression (lzma, N = 0..9): --compression lzma,N
## Only compress compressible data with lzma,N (N = 0..9): --compression auto,lzma,N
readonly COMPRESSION_ALGO='lz4'
readonly COMPRESSION_LEVEL='' ## must include comma: ',N'

## time,size,inode (default)
## mtime,size,inode (default behaviour of borg versions older than 1.1.0rc4)
## ctime,size (ignore the inode number)
## mtime,size (ignore the inode number)
## rechunk,ctime (all files are considered modified - rechunk, cache ctime)
## rechunk,mtime (all files are considered modified - rechunk, cache mtime)
## disabled (disable the files cache, all files considered modified - rechunk)
readonly FILECACHE="mtime,size"

## Number of days, weeks, &c. of backups to keep when pruning.
readonly KEEP_DAILY=7
readonly KEEP_WEEKLY=4
readonly KEEP_MONTHLY=6
readonly KEEP_YEARLY=1

## Setting this, so you won't be asked for your repository passphrase:
#export BORG_PASSPHRASE=''
## or this to ask an external program to supply the passphrase:
#export BORG_PASSCOMMAND=''

declare -A paths_to_backup=( \
  ["backup1"]='<path_to_backup1>' \
  ["backup2"]='<path_to_backup2>'
)

## Whitespace-separated list of paths to exclude from backup.
declare -A EXCLUDE=( \
    ["backup1"]='' \
    ["backup2"]=''
)


main() {
    #if [ `whoami` != root ]; then
    #    echo Please run this script as root or using sudo
    #    exit
    #fi

    if [ $# -lt 1 ] ;then
      usage
      exit 0
    fi


    parse_args "$@"
    exit 0
}


# $1...: command line arguments
parse_args() {
    DRYRUN=''

    while [ $# -gt 0 ]; do
        case $1 in
            -h|--help)
                usage
                exit 0
                ;;

            -l|--list)
                list
                exit 0
                ;;

            -i|--info)
                info
                exit 0
                ;;

             -dryrun)
                ## dry-run
                $DRYRUN="-n"
                shift
                ;;

            -init)
                init
                exit 0
                ;;

            -c|--create)
                if [[ $# -ne 2 ]] ; then
                    echo ' Error: You must tell me what you want to backup. The options are:'
                    for backup_id in "${!paths_to_backup[@]}"; do
                        echo "   $backup_id "
                    done
                    exit 0
                fi
                create $2
                exit 0

                ;;

            -p|--prune)
                prune
                exit 0
                ;;

            -d|--del)
                delete $@
                exit 0
                ;;

            -v|--check)
                check
                exit 0
                ;;

            -x)
                extract
                exit 0
                ;;
            -ulock)
                unlock
                exit 0
                ;;
             *)
                echo "Unknown argument: $1"
                HELP
                exit 1
                ;;
        esac
    done

    echo "Finished borg backup"
}

exit_error(){
    exit_error=$1
    exit_type=$2

    if [ ${exit_error} -eq 1 ];  then
        echo  " $exit_type finished with a warning"
    fi

    if [ ${exit_error} -gt 1 ];  then
        echo " $exit_type finished with an error"
        #exit $exit_error
    fi
}

usage() {
    echo
    echo "borg backup"
    printf "Usage: %s OPTION\n" "$(basename "$0")"
    printf "  %s\t%s\n" "-h|--help  " "print this help text and exit"
    printf "  %s\t%s\n" "-l|--list  " "list contents of repository"
    printf "  %s\t%s\n" "-i|--info  " "displays detailed info archive or repository"
    printf "  %s\t%s\n" "-init      " "initialize new repository"
    printf "  %s\t%s\n" "-dryrun    " "dry run"
    printf "  %s\t%s\n" "-c|--create" "create new archive"
    printf "  %s\t%s\n" "-p|--prune " "prune archive"
    printf "  %s\t%s\n" "-d|--del [TARGET::archive]" "delete a archive or repository"
    printf "  %s\t%s\n" "-v|--check " "verify repository consistency"
    printf "  %s\t%s\n" "-x         " "extracts the contents of an archive"
    echo
}

info() {
    echo "  REPO: $BORG_REPO"
    echo "TARGET: $TARGET"
    echo
    borg info "${TARGET}"
}

list() {
    borg list "${TARGET}"
}

init() {
    echo "Starting Borg repository initialization: ${TARGET}"

    borg init --encryption="${ENCRYPTION_METHOD}" "${TARGET}"

    echo "Finished Borg repository initialization ${TARGET}"
}

create() {
    backup_id=$1

    echo "Starting Borg archive creation: ${TARGET}"

    now=`date +%Y-%m-%d_%H:%M`

    name=${now}_${backup_id}
    echo " Starting backup: $name  ${paths_to_backup[$backup_id]}"

    borg create  \
        $DRYRUN  \
        --verbose  --progress \
        --list --filter AME   \
        --stats  --show-rc    \
        --compression "${COMPRESSION_ALGO}${COMPRESSION_LEVEL}"  \
        --exclude "${EXCLUDE[$backup_id]}"  \
        --ignore-inode \
        --files-cache "${FILECACHE}" \
        $TARGET::${name}      \
        ${paths_to_backup[$backup_id]}
    #backup_exit=$?
    #exit_error $backup_exit 'Backup'

    echo "Finished Borg archive creation: ${TARGET}"
}

prune() {
    echo "Better do prune manually"
    exit
    echo "Starting Borg prune: ${TARGET}"

    borg prune   \
        $DRYRUN  \
        --keep-daily="${KEEP_DAILY}" --keep-weekly="${KEEP_WEEKLY}" \
        --keep-monthly="${KEEP_MONTHLY}" --keep-yearly="${KEEP_YEARLY}" \
        "$TARGET"
    prune_exit=$?
    exit_error $prune_exit 'Prune'
    echo "Finished Borg prune: ${TARGET}"
}

delete() {
    echo $@
    if [[ $2 != "" ]]; then
        echo "WARNING: This will delete archive $2"
        printf "Are you sure you want to permanently delete the archive '%s'? [y/N] " $2
        read -r response
	#response='y'
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
            borg delete $TARGET::$2
            delete_exit=$?
            exit_error $delete_exit 'Delete'
            echo "Deleted Borg archive: ${2}"
        else
            printf "Aborted"
            exit 0
        fi
    else
        echo "WARNING: This will delete all repository"
        printf "Are you sure you want to permanently delete the repository '%s'? [y/N] " ${TARGET}
        read -r response
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
            #ssh "${USER}@${HOST}" rm -rf "${REPO}"
            borg delete $TARGET
            delete_exit=$?
            exit_error $delete_exit 'Delete'
            echo "Deleted Borg repository: ${TARGET}"
        else
            printf "Aborted"
            exit 0
        fi
    fi
}

check() {
    borg check --repair "${TARGET}"
}

extract(){
    echo
    echo " Currently, extract always writes into the current working directory (�.�), so make sure you cd to the right place before calling borg extract"
    echo
    echo "borg extract --list [--dry-run] $TARGET[::archive] [inside_directory] [--exclude '*.o']"
    echo
}

unlock(){
   borg break-lock "${TARGET}"
}

on_failure() {
    echo "Borg backup terminated unexpectedly"
}

trap on_failure SIGHUP SIGINT SIGTERM

main "$@"

